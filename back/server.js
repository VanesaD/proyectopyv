var express = require('express');
var app = express();
//para parsear el body del json
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port = process.env.PORT || 3000;

var baseMLabURL = "https://api.mlab.com/api/1/databases/techu_vdh/collections/";
var mLabAPIKey = "apiKey=eEVugPN9S1a7TP8AkPj9iCpUz_815xPy";
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto " + port);

/**
 * @api {get} /apitechu/v2/users Consulta de usuarios
 * @apiName users
 * @apiDescription Este método devuelve los usuario de la coleeción user
 *
 */
app.get('/apitechu/v2/users',
  function(req,res){
    console.log("GET /apitechu/v2/users");
    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");
    httpClient.get("user?" + mLabAPIKey,
      function(err, resMLab, body){
        var response = !err ? body : {
        "msg":"Error obteniendo usuarios."
        }
        res.send(response);
      }
    );
  }
);

/**
 * @api {get} /apitechu/v2/users/:id Consulta del usuario con id
 * @apiName users/:id
 * @apiDescription Este método devuelve el usuario con identificador id
 *
 */
app.get('/apitechu/v2/users/:id',
  function(req,res){
    console.log("GET /apitechu/v2/users/:id");
    var id = req.params.id;
    var query = 'q={"id": ' + id + '}';
    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Cliente creado");
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        if (err) {
          response = {"msg" : "Error obteniendo usuario."}
          res.status(500);
        }
        else {
          if (body.length > 0) {
            response = body[0];
          }
          else {
            response = {"msg" : "Usuario no encontrado."}
            res.status(404);
          }
        }
        res.send(response);
      }
    );
  }
);

/**
 * @api {post} /apitechu/v2/login Login de un usuario
 * @apiName login
 * @apiDescription Este método hace login del usuario con email y password introducios
 * por parámetro. Si el email es incorrecto o no existe, o la password es errónea devolvemos error
 *
 */
app.post('/apitechu/v2/login',
  function(req,res){
    console.log("POST /apitechu/v2/login");
    console.log("email es: " + req.body.email);
    console.log("password es: " + req.body.password);
    var query = 'q={"email": "' + req.body.email + '","password": "'+ req.body.password +'"}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion Cliente creada");
    var response = {"msg" : "mensaje salida"};
    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
        var encontrado = false;
        if (err) {
          response.msg = "Error obteniendo usuario.";
          res.status(500);
          res.send(response);
        }
        else {
          if (body.length > 0) {
            response.msg = "Usuario encontrado.";
            response.user = body[0];
            encontrado=true;
          }
          else {
            response.msg = "Usuario no encontrado.";
            res.status(404)
            res.send(response);
          }
        }
        if (encontrado){
          var query2 = 'q={"id": ' + body[0].id + '}';
          var putBody = '{"$set":{"logged":true}}';
          httpClient.put("user?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
          function(errPut, resMLabPut, bodyPut){
            if (errPut) {
                response.msg = "Error en logon.";
                res.status(500);
               }
            else {
              response.msg = "logon ok";
            }
            res.send(response);
          });
        }
       }
     );
  }
);

/**
 * @api {post} /apitechu/v2/:id/logout Logout del usuario con id
 * @apiName :id/logout
 * @apiDescription Este método hace logout del usuario con identificador id
 *
 */
app.post('/apitechu/v2/:id/logout',
  function(req,res){
    console.log("POST /apitechu/v2/:id/logout");
    console.log("id es: " + req.params.id);
    var query = 'q={"id": ' + req.params.id + '}';
    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion Cliente creada");
    var response = {"msg" : "mensaje salida"};
    httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMLab, body){
        var encontrado = false;
        if (err) {
          response.msg = "Error obteniendo usuario.";
          res.status(500);
          res.send(response);
        }
        else {
          console.log(body[0]);
          if (body.length > 0) {
            response.msg = "Usuario encontrado.";
            response.user = body[0];
            encontrado=true;
          }
          else {
            response.msg = "Usuario no encontrado.";
            res.status(404);
            res.send(response);
          }
        }
        if (encontrado){
          if (body[0].logged) { //Comprobamos si el usuario estaba logado
            var query2 = 'q={"id": ' + body[0].id + '}';
            var putBody = '{"$unset":{"logged":""}}';
            httpClient.put("user?" + query2 + "&" + mLabAPIKey, JSON.parse(putBody),
            function(errPut, resMLabPut, bodyPut){
              if (errPut) {
                response.msg = "Error en logout";
                res.status(500);
              }
              else {
                response.msg = "logout ok";
              }
              res.send(response);
            });
          }
        }
       }
     );
  }
);

/**
 * @api {get} /apitechu/v2/users/:id/accounts Consulta las cuentas de un usuario
 * @apiName users/:id/account
 * @apiDescription Este método devuelve las cuentas del usuario con identificador id
 *
 */
app.get('/apitechu/v2/users/:id/accounts',
  function(req,res){
    console.log("GET /apitechu/v2/users/:id/accounts");
    var user_id = req.params.id;
    var query = 'q={"user_id": ' + user_id + '}';
    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion creada");
    httpClient.get("account?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        if (err) {
          response = {"msg" : "Error obteniendo cuentas de usuario."}
          res.status(500);
        }
        else {
          if (body.length > 0) {
            response = body;
          }
          else {
            response = {"msg" : "Cuentas de usuario no encontradas."}
            res.status(404);
          }
        }
        res.send(response);
      }
    );
  }
);

/**
 * @api {get} /apitechu/v2/account/:id/movements Consulta los movimientos de una cuenta
 * @apiName account/:id/movements
 * @apiDescription Este método devuelve los movimientos de la cuenta con identificador id
 *
 */
app.get('/apitechu/v2/account/:id/movements',
  function(req,res){
    console.log("GET /apitechu/v2/account/:id/movements");
    var account_id = req.params.id;
    var query = 'q={"account_id": ' + account_id + '}' + '&s={"date":1,"movement_id":1}';
    httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion creada");
    httpClient.get("movement?" + query + "&" + mLabAPIKey,
      function(err, resMLab, body){
        if (err) {
          response = {"msg" : "Error obteniendo movimientos de cuenta."}
          res.status(500);
        }
        else {
          if (body.length > 0) {
            response = body;
          }
          else {
            response = {"msg" : "Movimientos de cuenta no encontrados."}
            res.status(404);
          }
        }
        res.send(response);
      }
    );
  }
);

/**
 * @api {post} /apitechu/v2/user Alta de un usuario
 * @apiName user
 * @apiDescription Este método realiza el alta de un usuario en la colección user con los campos:
 * first_name, last_name, email y password
 *
 */
app.post('/apitechu/v2/user',
  function(req,res){
    console.log("POST /v2/user");
    console.log("- first_name : " + req.body.first_name);
    console.log("- last_name : " + req.body.last_name);
    console.log("- email :" + req.body.email);
    console.log("- password : " + req.body.password);

    var httpClient = requestJson.createClient(baseMLabURL);
    var query = 'q={"email":"' + req.body.email + '"}'; //Comprobamos si existe algún usuario con el mismo email.
    var response = {"msg" : "mensaje salida"};
    httpClient.get("user?" + query + "&"+   mLabAPIKey,
      function(err, resMLab, body){
      var encontrado = true;
      if (err) {
        response.msg = "Error obteniendo usuario.";
        res.status(500);
        res.send(response);
      } else {
        if (body.length > 0) {
          response.msg = "GET - USUARIO EXISTENTE";
          res.status(404);
          res.send(response);
        } else {
          encontrado = false;
          response.msg = "GET - USUARIO NO ENCONTRADO";
        }
      }
      if(!encontrado){
        query = 's={"id":-1}&sk=0&l=1';
        httpClient.get("user?" + query + "&"+   mLabAPIKey,
        function(err, resMLab, body){
          var maxuser = false;
          if (err) {
            response.msg = "Error obteniendo máximo id.";
            res.status(500);
            res.send(response);
          } else {
            if (body.length > 0) {
              response = body[0];
              maxuser = true;
            } else {
              response.msg = "GET - ULTIMO USUARIO NO ENCONTRADO";
              res.status(404);
              res.send(response);
            }
          }
          if(maxuser){  //Damos de alta el usuario
              var newid = response.id + 1;
              var postBody = '{"id":'+ newid +',"first_name":"'+ req.body.first_name +'","last_name": "'+ req.body.last_name+'","email": "'+req.body.email +'","password": "'+ req.body.password +'"}';
              console.log("postBody - " + postBody);
              httpClient.post("user?"+ mLabAPIKey,JSON.parse(postBody),
              function(errPOST, resMLabPOST, bodyPOST) {
                if (errPOST) {
                  response.msg = "Error al hacer ALTA.";
                  res.status(500);
                } else {
                  response = postBody;
                }
                res.send(response);
              });
            }
        });
      }
    });
});

/**
 * @api {post} /apitechu/v2/user/account Alta de una cuenta
 * @apiName user/account
 * @apiDescription Este método realiza el alta de una cuenta cuando el usuario
 * realizar el alta en la aplicación
 *
 */
 app.post('/apitechu/v2/user/account',
  function(req,res){
    console.log("POST /v2/users/account");
    console.log("- id : " + req.body.id);
    var httpClient = requestJson.createClient(baseMLabURL);
    var query = 's={"account_id":-1}&sk=0&l=1';
    httpClient.get("account?" + query + "&"+   mLabAPIKey,
      function(err, resMLab, body){
        var maxaccount = false;
        var errorAlta = false;
        if (err) {
            response.msg = "Error obteniendo máximo id.";
            res.status(500);
            //res.send(response);
        } else {
            if (body.length > 0) {
              response = body[0];
              maxaccount = true;
            } else {
              response.msg = "GET - MÁXIMO ID CUENTA NO ENCONTRADO";
              res.status(404);
              //res.send(response);
            }
        }
        if(maxaccount){
          console.log("MaxId : " + response.account_id);
          //Damos de alta la cuenta
          var newid = response.account_id + 1;
          var numAle = getRandomInt(01,99);
          var alfaAle1 = getRandom(4);
          var alfaAle2 = getRandom(4);
          var alfaAle3 = getRandom(4);
          var alfaAle4 = getRandom(4);;
          var iban = "ES" + numAle + " 0182 " + alfaAle1 + " "+ alfaAle2 + " " + alfaAle3 + " " + alfaAle4;
          console.log("Código iban -->" + iban);
          var postBody = '{"account_id":'+ newid +',"user_id":'+ req.body.id +',"IBAN": "'+iban+'","balance": 50.01}';
          console.log("postBody - " +postBody);

          httpClient.post("account?"+   mLabAPIKey,JSON.parse(postBody),
            function(errPOST, resMLabPOST, bodyPOST) {
              if (errPOST) {
                response.msg = "Error al hacer ALTA.";
                res.status(500);
                errorAlta = true;
              } else {
                response = postBody;
                res.send(response);
              }
          });
        }
        if(!maxaccount || errorAlta){
          var deleteBody = '[]';    //es un put pero con el body en vacio []
          var query = 'q={"id":' + req.body.id + '}';
          httpClient.put("user?"+ query+"&"+mLabAPIKey,JSON.parse(deleteBody),
            function(err, resMLab, body){
              if (err) {
                response = {"msg" : "Error borrado usuario."};
                res.status(500);
              } else {
                response = {"msg" : "Error alta cuenta. Usuario borrado"};
              }
              res.send(response);
            });
        }
    });
});

/**
* Genera un número aleatorio
* @param {double} min valor mínimo
* @param {double} max valor máximo
* @returns {double} número aleatorio
*/
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

/**
* Genera un número aleatorio
* @param {integer} longitud longitud del valor aleatorio
* @returns {String} valor alfanumérico generado
*/
function getRandom(longitud)
{
  var caracteres = "ABCDEFGHIJKLMNPQRTUVWXYZ123456789";
  var salida = "";
  for (i=0; i<longitud; i++) salida += caracteres.charAt(Math.floor(Math.random()*caracteres.length));
  return salida;
}

/**
 * @api {post} /apitechu/v2/altamov Alta de un movimiento
 * @apiName altamov
 * @apiDescription Este método realiza el alta de un movimiento asociado a una cuenta
 *
 */
app.post('/apitechu/v2/altamov',
  function(req,res){
    console.log("POST /apitechu/v2/altamov");
    var hoy = new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();

    if(dd<10) {
      dd='0'+dd
    }

    if(mm<10) {
      mm='0'+mm
    }

    hoy = yyyy+'/'+mm+'/'+dd;

    console.log("Datos de entrada del nuevo movimiento:");
    console.log("- account_id: " + req.body.account_id);
    console.log("- description : " + req.body.description);
    console.log("- amount :" + req.body.amount);
    console.log("- date : " + hoy);

    var httpClient = requestJson.createClient(baseMLabURL);
    console.log("Conexion Cliente creada");

    var response = {"msg" : "mensaje salida"};
    query = 's={"movement_id":-1}&sk=0&l=1';
    httpClient.get("movement?" + query + "&"+   mLabAPIKey,
      function(err, resMLab, body){
        var maxmov = false;
        var alta = false;
        if (err) {
          response.msg = "Error obteniendo máximo movement_id.";
          res.status(500);
          res.send(response);
        } else {
          if (body.length > 0) {
            maxmov = true;
            response.movement= body[0];
            console.log(body[0]);
          } else {
            response.msg = "Maximo movement_id no encontrado";
            res.status(404);
            res.send(response);
          }
        }
        if(maxmov){ //Damos de alta el movimiento
          var newmov = response.movement.movement_id + 1;
          var postBody = '{"movement_id":'+ newmov +',"account_id":'+ req.body.account_id +',"description": "'+ req.body.description+'","amount": '+req.body.amount +',"date": "'+ hoy +'"}';
          console.log("postBody - " +postBody);
          httpClient.post("movement?"+  mLabAPIKey,JSON.parse(postBody),
            function(errPOST, resMLabPOST, bodyPOST) {
              if (errPOST) {
                response.msg = "Error al hacer Alta de Movimientos.";
                res.status(500);
                res.send(response);
              } else {
                alta = true;
                response.movement = bodyPOST;
              }
              if (alta){
                var query2 = 'q={"account_id": ' + req.body.account_id +'}';
                httpClient.get("account?" + query2 + "&" + mLabAPIKey,
                  function(err2, resMLab2, body2){
                    var encontrado = false;
                    var errorBalance = false;
                    if (err2) {
                      response.msg = "Error obteniendo cuenta.";
                      res.status(500);
                      //res.send(response);
                    }
                    else {
                      if (body.length > 0) {
                        response.msg = "Cuenta encontrada.";
                        response.account = body2[0];
                        encontrado=true;
                      }
                      else {
                        response.msg = "Cuenta no encontrada.";
                        res.status(404);
                        //res.send(response);
                      }
                    }
                    if (encontrado){
                      var query3 = 'q={"account_id": ' + body2[0].account_id + '}';
                      var newbalance = (body2[0].balance + response.movement.amount).toFixed(2);
                      var putBody = '{"$set":{"balance":' + newbalance + '}}';
                      httpClient.put("account?" + query3 + "&" + mLabAPIKey, JSON.parse(putBody),
                        function(errPut, resMLabPut, bodyPut){
                          if (errPut) {
                            response.msg = "Error en actualizacion balance.";
                            res.status(500);
                            errorBalance = true;
                          }
                          else {
                            response.msg = "Actualizacion balance ok";
                            response.account.balance = parseFloat(newbalance);
                            res.send(response);
                          }
                        }
                      );
                    }
                    if(!encontrado || errorBalance){
                      var deleteBody = '[]';
                      var query = 'q={"movement_id":' + newmov + '}';
                      console.log("movement?"+ query+"&"+mLabAPIKey);
                      console.log("body : " +deleteBody);
                      httpClient.put("movement?"+ query+"&"+mLabAPIKey,JSON.parse(deleteBody),  //es un put pero con el body en vacio []
                      function(errD, resMLabD, bodyD){
                        if (errD) {
                          response = {"msg" : "Error borrando movimiento."};
                          res.status(500);
                        } else {
                          console.log("Borrado movimiento --> " + newmov);
                          response = {"msg" : "Error actualizacion saldo cuenta. Movimiento borrado"};
                        }
                        res.send(response);
                      });
                    }
                  }
                );
              }
            }
          );
        }
      }
    );
  }
);
